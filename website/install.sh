#!/bin/bash
set -e # exit on any nonzero return value
set -x # echo each command as its being run

# install requirements
sudo apt install python3 python3-pip python3-setuptools python3-venv apache2 libapache2-mod-wsgi-py3 python3-zmq -y

sudo mkdir -p /var/www/gong_control

pushd /var/www/gong_control

sudo python3 -m venv venv
sudo chown -R $USER:$USER venv
pushd venv/bin
wget https://raw.githubusercontent.com/naztronaut/RaspberryPi-RGBW-Control/master/utils/activate_this.py
popd # return to /var/www/gong_control
. venv/bin/activate
python3 -m pip install flask requests
python3 -m pip install pyzmq
python3 -m pip install --upgrade luma.oled

pushd /home/$USER/gongclock/py_gong_driver/
pip3 install -e .
popd # back to /var/www/gong_control

deactivate

popd # back to /home/gong/gongclock/website
sudo a2ensite gong_control_server.conf
sudo a2dissite 000-default.conf
sudo systemctl enable apache2

./update_site.sh
