$(document).ready(function() {

    $('#setTimerBtn').on('click', function(e){
        // Get the value from the input field
        let hours = document.getElementById('setTimerHours').value;
        let minutes = document.getElementById('setTimerMinutes').value;
        let seconds = document.getElementById('setTimerSeconds').value;
        let volume = document.getElementById('setGongVolume').value; // [1-10]

        $.ajax({
            url: '/gong/set-timer?hours=' + hours + '&minutes=' + minutes + '&seconds=' + seconds,
            method: 'GET',
        }).done(function(response1) {

            $.ajax({
                url: '/gong/start-timer',
                method: 'GET',
            }).done(function(response2) {

                $.ajax({
                    url: '/gong/set-alert-volume?volume=' + volume,
                    method: 'GET',
                }).done(function(response3) {

                    console.log(response1)
                    console.log(response2)
                    console.log(response3)
                });
            });
        });
        e.preventDefault();
    });

    $('#stopBtn').on('click', function(e){
        
        $.ajax({
            url: '/gong/stop-timer',
            method: 'GET',
            success: function(result) {
                console.log(result);
         }
        });
        e.preventDefault();
    });

    $('#resumeBtn').on('click', function(e){
        
        $.ajax({
            url: '/gong/resume-timer',
            method: 'GET',
            success: function(result) {
                console.log(result);
         }
        });
        e.preventDefault();
    });

    function updateTimerState() {
        $.ajax({
            url: '/gong/get-timer',
            method: 'GET',
            success: function(result) {
                console.log(result);
                let data = JSON.parse(result);
                
                let timer_remaining_sec = Math.floor(data.timer_remaining_sec);
                let remaining_hours = Math.floor(timer_remaining_sec / (60 * 60));
                let remaining_minutes = Math.floor((timer_remaining_sec - remaining_hours*60*60) / 60);
                let remaining_seconds = timer_remaining_sec % 60;
                
                let total_timer_sec = data.timer_val_sec;
                let total_hours = Math.floor(total_timer_sec / (60 * 60));
                let total_minutes = Math.floor((total_timer_sec - total_hours*60*60) / 60);
                let total_seconds = total_timer_sec % 60;
                
                let timer_name = data.name
                let timer_message = data.message

                document.getElementById('timer-name').textContent = timer_name;

                if(data.timer_remaining_sec < 3){
                    document.getElementById('timer-message').textContent = timer_message;
                    document.getElementById('timer-message').style.opacity = 1;
                    setTimeout(function() {
                        document.getElementById('timer-message').style.opacity = 0;
                    }, 6000); // 10 seconds in milliseconds
                }

                document.getElementById('timer-remaining').textContent = remaining_hours + ":" + (remaining_minutes < 10 ? "0" : "") + remaining_minutes + ":" +(remaining_seconds < 10 ? "0" : "") + remaining_seconds;
                document.getElementById('timer-set-value').textContent = total_hours + ":" + (total_minutes < 10 ? "0" : "") + total_minutes + ":" +(total_seconds < 10 ? "0" : "") + total_seconds;

                if(data.running == true)
                {
                    $('#setTimerBtn').prop('disabled', true);
                    $('#resumeBtn').prop('disabled', true);
                    $('#stopBtn').prop('disabled', false);
                    $('#timer-remaining').removeClass('text-danger').addClass('text-success');
                }
                else
                {
                    $('#setTimerBtn').prop('disabled', false);
                    $('#resumeBtn').prop('disabled', false);
                    $('#stopBtn').prop('disabled', true);
                    $('#timer-remaining').removeClass('text-success').addClass('text-danger');
                }
         }
        });
    }

    function updateFormInputs() {
        $.ajax({
            url: '/gong/get-timer',
            method: 'GET',
        }).done(function(timer_dict) {

            $.ajax({
                url: '/gong/get-alert-volume',
                method: 'GET',
            }).done(function(volume_dict) {

                console.log(timer_dict);
                let data = JSON.parse(timer_dict);

                let total_timer_sec = data.timer_val_sec;
                let total_hours = Math.floor(total_timer_sec / (60 * 60));
                let total_minutes = Math.floor((total_timer_sec - total_hours*60*60) / 60);
                let total_seconds = total_timer_sec % 60;

                document.getElementById('setTimerHours').value = total_hours;
                document.getElementById('setTimerMinutes').value = total_minutes;
                document.getElementById('setTimerSeconds').value = total_seconds;
                
                console.log(volume_dict);
                let volume = JSON.parse(volume_dict);
                document.getElementById('setGongVolume').value = volume.volume
            });
        });
        
    }

    // Call updateTimerState on page load / refocus / reload
    updateTimerState();
    $(window).on('focus', updateTimerState);

    // Call updateFormInputs on page load / reload
    updateFormInputs();

    // Call updateTimerState every 1 seconds
    setInterval(updateTimerState, 1000);
});