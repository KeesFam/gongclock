$(document).ready(function() {

    $('#startBtn').on('click', function(e){
        
        $.ajax({
            url: '/gong/start-timer',
            method: 'GET',
            success: function(result) {
                console.log(result);
         }
        });
        e.preventDefault();
    });

    $('#stopBtn').on('click', function(e){
        
        $.ajax({
            url: '/gong/stop-timer',
            method: 'GET',
            success: function(result) {
                console.log(result);
         }
        });
        e.preventDefault();
    });

    $('#resumeBtn').on('click', function(e){
        
        $.ajax({
            url: '/gong/resume-timer',
            method: 'GET',
            success: function(result) {
                console.log(result);
         }
        });
        e.preventDefault();
    });

    $('#resetBtn').on('click', function(e){
        
        $.ajax({
            url: '/gong/reset-timer',
            method: 'GET',
            success: function(result) {
                console.log(result);
         }
        });
        e.preventDefault();
    });

    $('#setTimerBtn').on('click', function(e){
        // Get the value from the input field
        let hours = document.getElementById('setTimerHours').value;
        let minutes = document.getElementById('setTimerMinutes').value;;
        let seconds = document.getElementById('setTimerSeconds').value;; 

        $.ajax({
            url: '/gong/set-timer?hours=' + hours + '&minutes=' + minutes + '&seconds=' + seconds,
            method: 'GET',
            success: function(result) {
                console.log(result);
         }
        });
        e.preventDefault();
    });

    $('#setTimerNameBtn').on('click', function(e){
        // Get the value from the input field and convert into something that can be sent via a URL
        // e.g. with spaces replaced with %20
        let name = encodeURIComponent(document.getElementById('setTimerName').value);

        $.ajax({
            url: '/gong/set-timer-name?name=' + name,
            method: 'GET',
            success: function(result) {
                console.log(result);
         }
        });
        e.preventDefault();
    });

    $('#setTimerMessageBtn').on('click', function(e){
        // Get the value from the input field and convert into something that can be sent via a URL
        // e.g. with spaces replaced with %20
        let message = encodeURIComponent(document.getElementById('setTimerMessage').value);

        $.ajax({
            url: '/gong/set-timer-message?message=' + message,
            method: 'GET',
            success: function(result) {
                console.log(result);
         }
        });
        e.preventDefault();
    });

    $('#setAlertVolumeBtn').on('click', function(e){
        // Get the value from the input field and convert into something that can be sent via a URL
        // e.g. with spaces replaced with %20
        let volume = document.getElementById('setAlertVolumeValue').value;

        $.ajax({
            url: '/gong/set-alert-volume?volume=' + volume,
            method: 'GET',
            success: function(result) {
                console.log(result);
         }
        });
        e.preventDefault();
    });

    $('#getAlertVolumeBtn').on('click', function(e){

        $.ajax({
            url: '/gong/get-alert-volume',
            method: 'GET',
            success: function(result) {
                console.log(result);
                let volume = JSON.parse(result);
                
                document.getElementById('setAlertVolumeValue').value = volume["volume"];
            }
        });

        e.preventDefault();
    });

    $('#getTimerBtn').on('click', function(e){

        $.ajax({
            url: '/gong/get-timer',
            method: 'GET',
            success: function(result) {
                console.log(result);
                
                document.getElementById('showTimerInfo').textContent = result;
            }
        });

        e.preventDefault();
    });

    function updateTimerState() {
        $.ajax({
            url: '/gong/get-timer',
            method: 'GET',
            success: function(result) {
                console.log(result);
                let data = JSON.parse(result);
                
                let timer_remaining_sec = Math.floor(data.timer_remaining_sec);
                let remaining_hours = Math.floor(timer_remaining_sec / (60 * 60));
                let remaining_minutes = Math.floor((timer_remaining_sec - remaining_hours*60*60) / 60);
                let remaining_seconds = timer_remaining_sec % 60;
                
                let total_timer_sec = data.timer_val_sec;
                let total_hours = Math.floor(total_timer_sec / (60 * 60));
                let total_minutes = Math.floor((total_timer_sec - total_hours*60*60) / 60);
                let total_seconds = total_timer_sec % 60;
                
                let running = data.running;
                let timer_name = data.name
                let timer_message = data.message

                document.getElementById('timer-name').textContent = timer_name;
                document.getElementById('timer-message').textContent = timer_message;

                document.getElementById('timer-remaining').textContent = remaining_hours + ":" + (remaining_minutes < 10 ? "0" : "") + remaining_minutes + ":" +(remaining_seconds < 10 ? "0" : "") + remaining_seconds;
                document.getElementById('timer-set-value').textContent = total_hours + ":" + (total_minutes < 10 ? "0" : "") + total_minutes + ":" +(total_seconds < 10 ? "0" : "") + total_seconds;

                if(data.running == true)
                {
                    $('#startBtn').prop('disabled', true);
                    $('#resumeBtn').prop('disabled', true);
                    $('#stopBtn').prop('disabled', false);
                    $('#resetBtn').prop('disabled', false);
                    $('#timer-remaining').removeClass('text-danger').addClass('text-success');
                }
                else
                {
                    $('#startBtn').prop('disabled', false);
                    $('#resumeBtn').prop('disabled', false);
                    $('#stopBtn').prop('disabled', true);
                    $('#resetBtn').prop('disabled', false);
                    $('#timer-remaining').removeClass('text-success').addClass('text-danger');
                }
         }
        });
    }

    function updateFormInputs() {
        $.ajax({
            url: '/gong/get-timer',
            method: 'GET',
            success: function(result) {
                console.log(result);
                let data = JSON.parse(result);
                
                let total_timer_sec = data.timer_val_sec;
                let total_hours = Math.floor(total_timer_sec / (60 * 60));
                let total_minutes = Math.floor((total_timer_sec - total_hours*60*60) / 60);
                let total_seconds = total_timer_sec % 60;
                
                let timer_name = data.name
                let timer_message = data.message

                document.getElementById('setTimerHours').value = total_hours;
                document.getElementById('setTimerMinutes').value = total_minutes;
                document.getElementById('setTimerSeconds').value = total_seconds;

                document.getElementById('setTimerName').value = timer_name;
                document.getElementById('setTimerMessage').value = timer_message;

         }
        });
    }

    // Call updateTimerState on page load / refocus / reload
    updateTimerState();
    $(window).on('focus', updateTimerState);

    // Call updateFormInputs on page load / reload
    updateFormInputs();

    // Call updateTimerState every 1 seconds
    setInterval(updateTimerState, 1000);

    $('#pulseBtn').on('click', function(e){
        // Get the value from the input field
        let duration = document.getElementById('setPulseDuration').value; // ms
        let duty_cycle = document.getElementById('setPulseDutyCycle').value; // %
        let channel = document.getElementById('setPulseChannel').value; // [0,1]

        $.ajax({
            url: '/gong/pulse?duration=' + duration + '&duty-cycle=' + duty_cycle + '&channel=' + channel,
            method: 'GET',
            success: function(result) {
                console.log(result);
         }
        });
        e.preventDefault();
    });

    $('#gongBtn').on('click', function(e){
        // Get the value from the input field
        let volume = document.getElementById('setGongVolume').value; // [1-10]
        let channel = document.getElementById('setGongChannel').value; // [0,1]

        $.ajax({
            url: '/gong/gong?volume=' + volume +'&channel=' + channel,
            method: 'GET',
            success: function(result) {
                console.log(result);
         }
        });
        e.preventDefault();
    });

    $('#muteBtn').on('click', function(e){
        // Get the value from the input field
        let channel = document.getElementById('setMuteChannel').value; // [0,1]
        
        $.ajax({
            url: '/gong/mute?channel=' + channel,
            method: 'GET',
            success: function(result) {
                console.log(result);
         }
        });
        e.preventDefault();
    });

    $('#flushBtn').on('click', function(e){
        
        $.ajax({
            url: '/gong/flush',
            method: 'GET',
            success: function(result) {
                console.log(result);
         }
        });
        e.preventDefault();
    });
});