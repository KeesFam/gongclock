#!/bin/bash
set -e # exit on any nonzero return value
set -x # echo each command as its being run

sudo cp index.html /var/www/html/.
sudo cp script.js /var/www/html/.
sudo cp style.css /var/www/html/.

sudo cp random.html /var/www/html/.
sudo cp random_script.js /var/www/html/.
sudo cp random_style.css /var/www/html/.

sudo cp debug.html /var/www/html/.
sudo cp debug_script.js /var/www/html/.
sudo cp debug_style.css /var/www/html/.

# create symbolic links to allow accessing the page without the .html extension
sudo ln -sf /var/www/html/debug.html /var/www/html/debug
sudo ln -sf /var/www/html/random.html /var/www/html/random