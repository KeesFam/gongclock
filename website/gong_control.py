from flask import Flask, request, jsonify
import zmq
import logging
import json

logger = logging.getLogger(__name__)

app = Flask(__name__)

def get_zmq_ctx_socket():
    logger.debug("starting zmq connect")
    HOST = 'localhost'
    PORT = 5555
    zmq_ctx = zmq.Context().instance()
    socket = zmq_ctx.socket(zmq.REQ)
    socket.connect('tcp://{}:{}'.format(HOST, PORT))
    logger.debug("zmq connected")

    return (zmq_ctx, socket)

@app.route('/')
def welcome():
    return("Welcome to gong control!")

# /gong/pulse?duration=10&duty-cycle=5&channel=1
@app.route('/pulse', methods=['GET'])
def pulse():
    duration = request.args.get('duration')
    duty_cycle = request.args.get('duty-cycle')
    channel = request.args.get('channel')

    if duration == None:
        duration = 35
    if duty_cycle == None:
        duty_cycle = 100
    if channel == None:
        channel = 0
    
    try:
        duration = int(duration)
        duty_cycle = int(duty_cycle)
        channel = int(channel)
    except ValueError:
        duration = 35
        duty_cycle = 100
        channel = 0

    (zmq_ctx, socket) = get_zmq_ctx_socket()
    
    command = "pulse".encode("utf-8")
    args = json.dumps({"duration_ms" : duration, "duty_cycle" : duty_cycle, "channel" : channel, "freq_hz" : 297}).encode("utf-8")
    logger.debug("sending %s : %s", command, args)
    socket.send_multipart([command, args])
    logger.debug("sent")
    resp = socket.recv()
    logger.debug("got resp: %s", resp)

    socket.close()
    zmq_ctx.term()

    return(f"pulse at {duration} ms {duty_cycle} percent on channel {channel}")
    #return(jsonify())

#/gong/gong?volume=3&channel=0
@app.route('/gong', methods=['GET'])
def gong():
    volume = request.args.get('volume')
    channel = request.args.get('channel')
    
    if volume == None:
        volume = 5
    if channel == None:
        channel = 0
    
    try:
        volume = int(volume)
        channel = int(channel)
    except ValueError:
        volume = 5
        channel = 0

    (zmq_ctx, socket) = get_zmq_ctx_socket()
    
    command = "gong".encode("utf-8")
    args = json.dumps({"volume" : volume, "channel" : channel}).encode("utf-8")
    logger.debug("sending %s : %s", command, args)
    socket.send_multipart([command, args])
    logger.debug("sent")
    resp = socket.recv()
    logger.debug("got resp: %s", resp)

    socket.close()
    zmq_ctx.term()

    return(f"gong @ Volume : {volume} on channel {channel}")

#/gong/mute?channel=0
@app.route('/mute', methods=['GET'])
def mute():
    channel = request.args.get('channel')

    if channel == None:
        channel = 0
    
    try:
        channel = int(channel)
    except ValueError:
        channel = 0

    (zmq_ctx, socket) = get_zmq_ctx_socket()
    
    command = "mute".encode("utf-8")
    args = json.dumps({"channel" : channel}).encode("utf-8")
    logger.debug("sending %s : %s", command, args)
    socket.send_multipart([command, args])
    logger.debug("sent")
    resp = socket.recv()
    logger.debug("got resp: %s", resp)

    socket.close()
    zmq_ctx.term()

    return(f"Mute on channel {channel}")

#/gong/flush
@app.route('/flush', methods=['GET'])
def flush():

    (zmq_ctx, socket) = get_zmq_ctx_socket()
    
    command = "flush".encode("utf-8")
    args = json.dumps({}).encode("utf-8")
    logger.debug("sending %s : %s", command, args)
    socket.send_multipart([command, args])
    logger.debug("sent")
    resp = socket.recv()
    logger.debug("got resp: %s", resp)

    socket.close()
    zmq_ctx.term()

    return(f"Flush")

#/gong/set-timer?hours=0&minutes=0&seconds=10
@app.route('/set-timer', methods=['GET'])
def set_timer():
    hours = request.args.get('hours')
    minutes = request.args.get('minutes') 
    seconds = request.args.get('seconds') # time in seconds
    
    if hours == None:
        hours = 0
    if minutes == None:
        minutes = 0
    if seconds == None:
        seconds = 1
    
    try:
        hours = int(hours)
        minutes = int(minutes)
        seconds = int(seconds)
    except ValueError:
        hours = 0
        minutes = 0
        seconds = 1

    (zmq_ctx, socket) = get_zmq_ctx_socket()
    
    command = "set_timer".encode("utf-8")
    args = json.dumps({"hours" : hours, "minutes" : minutes, "seconds" : seconds}).encode("utf-8")
    logger.debug("sending %s : %s", command, args)
    socket.send_multipart([command, args])
    logger.debug("sent")
    resp = socket.recv()
    logger.debug("got resp: %s", resp)

    socket.close()
    zmq_ctx.term()

    return(f"timer set to {hours}:{minutes}:{seconds}")

#/gong/start-timer
@app.route('/start-timer', methods=['GET'])
def start_timer():

    (zmq_ctx, socket) = get_zmq_ctx_socket()
    
    command = "start".encode("utf-8")
    args = json.dumps({}).encode("utf-8")
    logger.debug("sending %s : %s", command, args)
    socket.send_multipart([command, args])
    logger.debug("sent")
    resp = socket.recv()
    logger.debug("got resp: %s", resp)

    socket.close()
    zmq_ctx.term()

    return(f"Timer started")

#/gong/resume-timer
@app.route('/resume-timer', methods=['GET'])
def resume_timer():

    (zmq_ctx, socket) = get_zmq_ctx_socket()
    
    command = "resume".encode("utf-8")
    args = json.dumps({}).encode("utf-8")
    logger.debug("sending %s : %s", command, args)
    socket.send_multipart([command, args])
    logger.debug("sent")
    resp = socket.recv()
    logger.debug("got resp: %s", resp)

    socket.close()
    zmq_ctx.term()

    return(f"Timer resumed")

#/gong/stop-timer
@app.route('/stop-timer', methods=['GET'])
def stop_timer():

    (zmq_ctx, socket) = get_zmq_ctx_socket()
    
    command = "stop".encode("utf-8")
    args = json.dumps({}).encode("utf-8")
    logger.debug("sending %s : %s", command, args)
    socket.send_multipart([command, args])
    logger.debug("sent")
    resp = socket.recv()
    logger.debug("got resp: %s", resp)

    socket.close()
    zmq_ctx.term()

    return(f"Timer stopped")

#/gong/reset-timer
@app.route('/reset-timer', methods=['GET'])
def reset_timer():

    (zmq_ctx, socket) = get_zmq_ctx_socket()
    
    command = "reset".encode("utf-8")
    args = json.dumps({}).encode("utf-8")
    logger.debug("sending %s : %s", command, args)
    socket.send_multipart([command, args])
    logger.debug("sent")
    resp = socket.recv()
    logger.debug("got resp: %s", resp)

    socket.close()
    zmq_ctx.term()

    return(f"Timer reset")

#/gong/set-timer-name?name=foo%20bar
@app.route('/set-timer-name', methods=['GET'])
def set_timer_name():
    name = request.args.get('name')
    
    if name == None:
        name = ""
    
    try:
        name = str(name)
    except ValueError:
        name = ""

    if name != "":
        (zmq_ctx, socket) = get_zmq_ctx_socket()
        
        command = "set_name".encode("utf-8")
        args = json.dumps({"name" : name}).encode("utf-8")
        logger.debug("sending %s : %s", command, args)
        socket.send_multipart([command, args])
        logger.debug("sent")
        resp = socket.recv()
        logger.debug("got resp: %s", resp)

        socket.close()
        zmq_ctx.term()

    return(f"Set timer name to {name}")

#/gong/set-timer-message?message=baz%20bop
@app.route('/set-timer-message', methods=['GET'])
def set_timer_message():
    message = request.args.get('message')
    
    if message == None:
        message = ""
    
    try:
        message = str(message)
    except ValueError:
        message = ""

    if message != "":
        (zmq_ctx, socket) = get_zmq_ctx_socket()
        
        command = "set_message".encode("utf-8")
        args = json.dumps({"message" : message}).encode("utf-8")
        logger.debug("sending %s : %s", command, args)
        socket.send_multipart([command, args])
        logger.debug("sent")
        resp = socket.recv()
        logger.debug("got resp: %s", resp)

        socket.close()
        zmq_ctx.term()

    return(f"Set timer message to {message}")

#/gong/get-timer
@app.route('/get-timer', methods=['GET'])
def get_timer():

    (zmq_ctx, socket) = get_zmq_ctx_socket()
    
    command = "get".encode("utf-8")
    args = json.dumps({}).encode("utf-8")
    logger.debug("sending %s : %s", command, args)
    socket.send_multipart([command, args])
    logger.debug("sent")
    resp = socket.recv()
    logger.debug("got resp: %s", resp)

    socket.close()
    zmq_ctx.term()

    return(resp)

#/gong/set-alert-volume?volume=3&channel=0
@app.route('/set-alert-volume', methods=['GET'])
def set_alert_volume():
    volume = request.args.get('volume')
    
    if volume == None:
        volume = 5
    
    try:
        volume = int(volume)
    except ValueError:
        volume = 5

    (zmq_ctx, socket) = get_zmq_ctx_socket()
    
    command = "set_alert_volume".encode("utf-8")
    args = json.dumps({"volume" : volume}).encode("utf-8")
    logger.debug("sending %s : %s", command, args)
    socket.send_multipart([command, args])
    logger.debug("sent")
    resp = socket.recv()
    logger.debug("got resp: %s", resp)

    socket.close()
    zmq_ctx.term()

    return(f"set alert volume to Volume : {volume}")

#/gong/get-alert-volume
@app.route('/get-alert-volume', methods=['GET'])
def get_alert_volume():

    (zmq_ctx, socket) = get_zmq_ctx_socket()
    
    command = "get_alert_volume".encode("utf-8")
    args = json.dumps({}).encode("utf-8")
    logger.debug("sending %s : %s", command, args)
    socket.send_multipart([command, args])
    logger.debug("sent")
    resp = socket.recv()
    logger.debug("got resp: %s", resp)

    socket.close()
    zmq_ctx.term()

    return(resp)

if __name__ == '__main__':
    app.run(debug=True)