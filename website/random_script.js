$(document).ready(function() {

    $('#setTimerBtn').on('click', function(e){
        // Get the value from the input field
        let hours = 0;
        let minutes = getRandomInt(15, 90);
        let seconds = 0;
        let volume = document.getElementById('setGongVolume').value; // [1-10]

        $.ajax({
            url: '/gong/set-timer?hours=' + hours + '&minutes=' + minutes + '&seconds=' + seconds,
            method: 'GET',
        }).done(function(response1) {

            $.ajax({
                url: '/gong/start-timer',
                method: 'GET',
            }).done(function(response2) {

                $.ajax({
                    url: '/gong/set-alert-volume?volume=' + volume,
                    method: 'GET',
                }).done(function(response3) {

                    console.log(response1)
                    console.log(response2)
                    console.log(response3)
                });
            });
        });
        e.preventDefault();
    });

    /**
     * Returns a random integer between min (inclusive) and max (inclusive).
     * The value is no lower than min (or the next integer greater than min
     * if min isn't an integer) and no greater than max (or the next integer
     * lower than max if max isn't an integer).
     * Using Math.round() will give you a non-uniform distribution!
     */
    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function updateTimerState() {
        $.ajax({
            url: '/gong/get-timer',
            method: 'GET',
            success: function(result) {
                console.log(result);
                let data = JSON.parse(result);
                
                let timer_name = data.name
                let timer_message = data.message

                document.getElementById('timer-name').textContent = timer_name;

                if(data.timer_remaining_sec < 3){
                    document.getElementById('timer-message').textContent = timer_message;
                    document.getElementById('timer-message').style.opacity = 1;
                    setTimeout(function() {
                        document.getElementById('timer-message').style.opacity = 0;
                    }, 6000); // 10 seconds in milliseconds
                }

                if(data.running == true)
                {
                    $('#setTimerBtn').prop('disabled', true);
                }
                else
                {
                    $('#setTimerBtn').prop('disabled', false);
                }
         }
        });
    }

    function updateFormInputs() {
        $.ajax({
            url: '/gong/get-timer',
            method: 'GET',
        }).done(function(timer_dict) {

            $.ajax({
                url: '/gong/get-alert-volume',
                method: 'GET',
            }).done(function(volume_dict) {

                console.log(timer_dict);
                let data = JSON.parse(timer_dict);
                
                console.log(volume_dict);
                let volume = JSON.parse(volume_dict);
                document.getElementById('setGongVolume').value = volume.volume
            });
        });
        
    }

    // Call updateTimerState on page load / refocus / reload
    updateTimerState();
    $(window).on('focus', updateTimerState);

    // Call updateFormInputs on page load / reload
    updateFormInputs();

    // Call updateTimerState every 1 seconds
    setInterval(updateTimerState, 1000);
});