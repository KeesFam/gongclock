#!/usr/bin/env python3
import logging
import sys

logger = logging.getLogger(__name__)

sh = logging.StreamHandler(sys.stderr)
sh.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(name)s:%(levelname)s: %(message)s')
sh.setFormatter(formatter)

logger.addHandler(sh)
logger.setLevel(logging.INFO)

gong_control_logger = logging.getLogger('gong_control')
gong_control_logger.addHandler(sh)
gong_control_logger.setLevel(logging.INFO)

driver_logger = logging.getLogger('py_gong_driver')
driver_logger.addHandler(sh)
driver_logger.setLevel(logging.INFO)

activate_this = '/var/www/gong_control/venv/bin/activate_this.py'
with open(activate_this) as file_:
	exec(file_.read(), dict(__file__=activate_this))
	
import sys
sys.path.insert(0, '/var/www/gong_control')

from gong_control import app as application
