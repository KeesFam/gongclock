#!/bin/bash
set -e # exit on any nonzero return value
set -x # echo each command as its being run

sudo cp gong_control.py /var/www/gong_control/.
sudo cp gong_control.wsgi /var/www/gong_control/.
sudo cp gong_control_server.conf /etc/apache2/sites-available/.

./update_html.sh

sudo systemctl daemon-reload && sudo systemctl restart apache2