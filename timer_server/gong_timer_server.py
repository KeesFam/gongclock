import time
import zmq
import logging
import threading
import signal
import datetime
import py_gong_driver
import json
import Timer

logger = logging.getLogger(__name__)

class Timer_Server():
    def __init__(self):
        self.threads = []
        self.driver = py_gong_driver.PulseDriver(297)
        self.timer = Timer.Timer(hours = 1, minutes = 0, seconds = 0, name = "Meditation Timer", message = "Namaste", alarm_id = None)
        self.volume = 3

    def worker_routine(self, worker_url_work: str, worker_url_control: str, context: zmq.Context = None):
        """The worker thread"""
        context = context or zmq.Context.instance()

        #Sockets to talk to the dispatcher
        rep_socket = context.socket(zmq.REP)
        rep_socket.connect(worker_url_work)

        control_socket = context.socket(zmq.SUB)
        control_socket.connect(worker_url_control)
        control_socket.setsockopt(zmq.SUBSCRIBE, b"")

        poller = zmq.Poller()
        poller.register(rep_socket, zmq.POLLIN)
        poller.register(control_socket, zmq.POLLIN)

        logger.info("Req/Rep thread started")

        while True:
            socks = dict(poller.poll())

            if socks.get(rep_socket) == zmq.POLLIN:
                [command, args] = rep_socket.recv_multipart()
                command = command.decode("utf-8")
                args = args.decode("utf-8")
                logger.debug("received [command, args]: %s : %s", command, args)

                if command in ["pulse", "gong", "flush", "mute"]:
                    resp = self.driver_handler(command, args)
                
                elif command in ["start", "resume", "stop", "set_timer", "reset", "get" , "set_name", "set_message", "set_alert_volume", "get_alert_volume"]:
                    resp = self.timer_handler(command, args)

                else:
                    logger.error("Received unrecognized command: %s %s", command, args)
                    resp = "fail".encode("utf-8")

                rep_socket.send(resp)

            if socks.get(control_socket) == zmq.POLLIN:
                logger.debug("Req/Rep thread got exit")
                break
        
        rep_socket.close()
        control_socket.close()
        logger.info("Req/Rep thread closed")

    def timer_handler(self, command, args):
        """handle the commands meant to control the timer"""
        
        resp = "ok".encode("utf-8")

        if command == "start":
            logger.debug("got start command")
            try:
                self.timer.start()
                # if the power supply has been off for a while, we need to draw some power
                # in order to get it charged up, but we want it to be silent.
                # 500 ms at 10% duty cycle seems to work
                self.driver.pulse(500, 10, 0)

                # gong to indicate timer started
                self.driver.gong(volume = self.volume)
            except Exception as e:
                logger.error("encountered error: %s", e)
        elif command == "resume":
            logger.debug("got resume command")
            try:
                self.timer.resume()
                # if the power supply has been off for a while, we need to draw some power
                # in order to get it charged up, but we want it to be silent.
                # 500 ms at 10% duty cycle seems to work
                self.driver.pulse(500, 10, 0)

                # gong to indicate timer started
                self.driver.gong(volume = self.volume)
            except Exception as e:
                logger.error("encountered error: %s", e)
        elif command == "stop":
            logger.debug("got stop command")
            try:
                self.timer.stop()
            except Exception as e:
                logger.error("encountered error: %s", e)
        elif command == "set_timer":
            logger.debug("got set_timer command: %s", args)
            try:
                args = json.loads(args)
                self.timer.set(hours = args["hours"], minutes = args["minutes"], seconds = args["seconds"])
            except json.JSONDecodeError as e:
                logger.error("Failed to decode json: %s", args)
            except Exception as e:
                logger.error("encountered error: %s", e)
        elif command == "set_name":
            logger.debug("got set_name command: %s", args)
            try:
                args = json.loads(args)
                self.timer.name = args["name"]
            except json.JSONDecodeError as e:
                logger.error("Failed to decode json: %s", args)
            except Exception as e:
                logger.error("encountered error: %s", e)
        elif command == "set_message":
            logger.debug("got set_message command: %s", args)
            try:
                args = json.loads(args)
                self.timer.message = args["message"]
            except json.JSONDecodeError as e:
                logger.error("Failed to decode json: %s", args)
            except Exception as e:
                logger.error("encountered error: %s", e)
        elif command == "reset":
            logger.debug("got reset command")
            try:
                self.timer.reset()
            except Exception as e:
                logger.error("encountered error: %s", e)
        elif command == "get":
            logger.debug("got get command")
            try:
                resp = json.dumps(self.timer.get_dict()).encode("utf-8")
            except Exception as e:
                logger.error("encountered error: %s", e)
        elif command == "set_alert_volume":
            logger.debug("got set_alert_volume command: %s", args)
            try:
                args = json.loads(args)
                self.volume = args["volume"]
            except json.JSONDecodeError as e:
                logger.error("Failed to decode json: %s", args)
            except Exception as e:
                logger.error("encountered error: %s", e)
        elif command == "get_alert_volume":
            logger.debug("got get_alert_volume command: %s", args)
            try:
                resp = json.dumps({"volume" : self.volume}).encode("utf-8")
            except json.JSONDecodeError as e:
                logger.error("Failed to decode json: %s", args)
            except Exception as e:
                logger.error("encountered error: %s", e)
        else:
            logger.error("got unexpected command: %s : %s", command, args)
            #raise ValueError

        return resp

    def driver_handler(self, command, args):
        """handle the commands meant for the py_gong_driver directly"""

        resp = "ok".encode("utf-8")
        if command == "pulse":
            logger.debug("got pulse command")
            try:
                args = json.loads(args)
                self.driver.pulse(duration = args["duration_ms"], duty_cycle = args["duty_cycle"], channel = args["channel"])
            except json.JSONDecodeError as e:
                logger.error("Failed to decode json: %s", args)
            except Exception as e:
                logger.error("encountered error: %s", e)

        elif command == "flush":
            logger.debug("got flush command")
            try:
                self.driver.flush()
            except Exception as e:
                logger.error("encountered error: %s", e)

        elif command == "gong":
            logger.debug("got gong command")
            try:
                args = json.loads(args)
                self.driver.gong(volume = args["volume"], channel = args["channel"])
            except json.JSONDecodeError as e:
                logger.error("Failed to decode json: %s", args)
            except Exception as e:
                logger.error("encountered error: %s", e)

        elif command == "mute":
            logger.debug("got mute command")
            try:
                args = json.loads(args)
                self.driver.mute(channel = args["channel"])
            except json.JSONDecodeError as e:
                logger.error("Failed to decode json: %s", args)
            except Exception as e:
                logger.error("encountered error: %s", e)
        else:
            logger.error("got unexpected command: %s : %s", command, args)
            #raise ValueError

        return resp

    def timer_publisher(self, worker_url_pub: str, worker_url_control: str, context: zmq.Context = None):
        """publish the timer updates"""
        context = context or zmq.Context.instance()

        #Sockets to talk to the dispatcher
        pub_socket = context.socket(zmq.PUB)
        pub_socket.bind(worker_url_pub)

        control_socket = context.socket(zmq.SUB)
        control_socket.connect(worker_url_control)
        control_socket.setsockopt(zmq.SUBSCRIBE, b"")

        poller = zmq.Poller()
        poller.register(control_socket, zmq.POLLIN)

        logger.info("Pub thread started")

        while True:
            # poll, do not wait if nothing is there
            socks = dict(poller.poll(0))

            if socks.get(control_socket) == zmq.POLLIN:
                logger.debug("Pub thread got exit")
                break
            
            # handle date and time publications
            str1 = datetime.date.today().strftime("%B %d, %Y")
            str2 = datetime.datetime.now().strftime("%H:%M:%S")
            pub_socket.send_multipart(["date".encode("utf-8"), str1.encode("utf-8")])
            pub_socket.send_multipart(["time".encode("utf-8"), str2.encode("utf-8")])

            # handle the timer
            if self.timer.running:
                # update timer and check if expired
                if self.timer.is_expired():
                    self.timer.reset()
                    # gong here and announce
                    timer_obj = json.dumps(self.timer.get_dict()).encode("utf-8")
                    pub_socket.send_multipart(["timer_complete".encode("utf-8"), timer_obj])
                    
                    # if the power supply has been off for a while, we need to draw some power
                    # in order to get it charged up, but we want it to be silent.
                    # 500 ms at 10% duty cycle seems to work
                    self.driver.pulse(500, 10, 0)

                    # first gong and delay
                    self.driver.gong(volume = self.volume)
                    self.driver.pulse(3000, 0, 0)

                    # second gong and delay
                    self.driver.gong(volume = self.volume)
                    self.driver.pulse(3000, 0, 0)

                    # third gong
                    self.driver.gong(volume = self.volume)

            # publish timer information
            if self.timer.timer_remaining_sec < 0:
                (h, m, s) = (0, 0, 0)
            else:
                (h, m, s) = self.timer.calc_time(self.timer.timer_remaining_sec)
            remaining_time = {"hours" : h, "minutes" : m, "seconds" : s}
            remaining_time = json.dumps(remaining_time).encode("utf-8")
            pub_socket.send_multipart(["remaining_time".encode("utf-8"), remaining_time])
            
            timer_obj = json.dumps(self.timer.get_dict()).encode("utf-8")
            pub_socket.send_multipart(["timer_status".encode("utf-8"), timer_obj])

            time.sleep(1)
        
        pub_socket.close()
        control_socket.close()
        logger.info("Pub thread closed")


    def server(self):
        """Server routine"""
        url_worker_rep = "inproc://workers"
        url_worker_control = "inproc://control"
        url_client = "tcp://*:5555"
        url_worker_pub = "tcp://*:5556"
        #url_client = "ipc:///tmp/gong_timer_server_0"

        self.context = zmq.Context.instance()

        #socket to talk to clients
        self.clients = self.context.socket(zmq.ROUTER)
        self.clients.bind(url_client)

        #socket to talk to workers
        self.workers = self.context.socket(zmq.DEALER)
        self.workers.bind(url_worker_rep)

        self.controller = self.context.socket(zmq.PUB)
        self.controller.bind(url_worker_control)

        # start req/rep threads
        for i in range(2):
            thread = threading.Thread(target=self.worker_routine, args=(url_worker_rep, url_worker_control, ))
            thread.deamon = True
            logger.info("Starting thread %d", i)
            thread.start()
            self.threads.append(thread)

        # start publisher thread
        thread = threading.Thread(target=self.timer_publisher, args=(url_worker_pub, url_worker_control, ))
        thread.deamon = True
        logger.info("Starting publisher thread")
        thread.start()
        self.threads.append(thread)

        # handle SIGTERM from something like systemd
        signal.signal(signal.SIGTERM, self.exit_gracefully_signal)

        try:
            zmq.proxy(self.clients, self.workers)
        except KeyboardInterrupt:
            logger.warning("Received keyboard interrupt, stopping server")
        finally:
            self.exit_gracefully()

    def exit_gracefully_signal(self, signum, frame):
        logger.info("Got terminate signal")
        raise KeyboardInterrupt

    def exit_gracefully(self):
        self.controller.send(b"KILL")
        for num, thread in enumerate(self.threads):
            thread.join()
            logger.info("joinging thread %d", num)
        self.clients.close()
        self.workers.close()
        self.controller.close()
        self.context.term()
        self.driver.shutdown()
        logger.info("Done exiting gracefully from keyboard")

if __name__ == "__main__":
    verbosity = 2
    sh = logging.StreamHandler()
    sh.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(name)s:%(levelname)s: %(message)s')
    sh.setFormatter(formatter)

    logger.addHandler(sh)
    logger.setLevel(verbosity*10)

    driver_logger = logging.getLogger('py_gong_driver')
    driver_logger.addHandler(sh)
    driver_logger.setLevel(verbosity*10)

    Server = Timer_Server()
    Server.server()
