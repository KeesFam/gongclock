import zmq
import logging
import json
import time

logger = logging.getLogger(__name__)

context = zmq.Context()

from luma.core.interface.serial import i2c
from luma.core.render import canvas
from luma.oled.device import ssd1309
serial = i2c(port=1, address=0x3C)
device = ssd1309(serial)

def client():
    # socket to talk to server
    logger.info("connecting to gong timer server ...")
    socket = context.socket(zmq.SUB)
    socket.connect("tcp://localhost:5556")
    #socket.connect("tcp://192.168.1.222:5556")
    socket.setsockopt(zmq.SUBSCRIBE, b"")
    logger.info("connected")
    device.contrast(1) # lowest "brightness".  It's still pretty bright though

    display_on = False
    
    try:
        str1 = "" # date
        str2 = "" # time
        str3 = "" # meditation time name
        str4 = "" # meditation time remaining time
        str5 = "" # timer complete message
        str6 = "" # timer running status
        time_then = time.time()
        while True:
            time.sleep(0.5)
            if time.time() - time_then > 3 : str5 = ""
            try:
                while True:
                    [address, contents] = socket.recv_multipart(flags=zmq.NOBLOCK)
                    logger.debug(f"[ {address} ] {contents}")
                    if address == b'date' : str1 = contents.decode("utf-8")
                    if address == b'time' : str2 = contents.decode("utf-8")
                    if address == b'timer_complete':
                        timer_dict = json.loads(contents.decode("utf-8"))
                        str5 = timer_dict["message"]
                        time_then = time.time()
                    if address == b'timer_status':
                        timer_dict = json.loads(contents.decode("utf-8"))
                        str6 = "stopped" if timer_dict["running"] == False else "running"
                        str3 = timer_dict["name"]
                    if address == b'remaining_time':
                        timer_dict = json.loads(contents.decode("utf-8"))
                        minutes = timer_dict["minutes"]
                        seconds = timer_dict["seconds"]
                        if minutes < 10:
                            minutes_str = "0" + str(minutes)
                        else:
                            minutes_str = str(minutes)
                        if seconds < 10:
                            seconds_str = "0" + str(seconds)
                        else:
                            seconds_str = str(seconds)
                        str4 = f'{timer_dict["hours"]}:{minutes_str}:{seconds_str}'
            except zmq.Again as e:
                # nothing in queue, continue
                pass
            
            if timer_dict["running"] == False:
                if time.time() - time_then > 600 : # leave the display on after the timer expires for 10 minutes
                    if display_on == True:
                        device.hide()
                        display_on = False
            else:
                if display_on == False:
                    display_on = True
                    device.show()
                with canvas(device) as draw:
                    #draw.rectangle(device.bounding_box, outline="white", fill="black")
                    draw.text((5, 0), f"{str1}", fill="white")
                    draw.text((5, 10), f"{str2}", fill="white")
                    draw.text((5, 20), f"{str3}", fill="white")
                    draw.text((5, 30), f"{str4}", fill="white")
                    draw.text((5, 40), f"{str5}", fill="white")
                    draw.text((5, 50), f"{str6}", fill="white")

    except KeyboardInterrupt:
        logger.debug("got keyboard interrupt")
    finally:
        socket.close()
        context.term()
        logger.info("client timer watcher shut down")


if __name__ == "__main__":
    verbosity = 2
    sh = logging.StreamHandler()
    sh.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(name)s:%(levelname)s: %(message)s')
    sh.setFormatter(formatter)

    logger.addHandler(sh)
    logger.setLevel(verbosity*10)

    client()
