import time
import logging
import uuid
import typing

logger = logging.getLogger(__name__)

class Timer(dict):
    def __init__(self, hours: int = 0, minutes: int = 0, seconds: int = 0, name: str = "", message: str = "", alarm_id: str = None):
        if hours < 0 or minutes < 0 or seconds < 0:
            raise ValueError
        self._running = False
        self._timer_val_sec = hours * 60 * 60 + minutes * 60 + seconds
        self._timer_remaining_sec = self._timer_val_sec
        self._name = name
        self._message = message
        self._id = uuid.uuid4().hex
        self._id_next = None # expects uuid4 or None
        self._alarm_id = alarm_id # expects uuid4 or None
        # internal state variable
        self._last_checked_time = 0
        logger.info("%d:%d:%d timer created", hours, minutes, seconds)
        logger.debug(self.__repr__())

    def set(self, hours: int = 0, minutes: int = 0, seconds: int = 0):
        """sets the timer value.  If the timer is running, resets the elapsed time to the new value and continues running"""
        if hours < 0 or minutes < 0 or seconds < 0:
            logger.error("Tried to set a negative value for timer %d:%d:%d", hours, minutes, seconds)
            raise ValueError
        
        if (hours + minutes + seconds) == 0:
            logger.warn("Tried to set zero time timer, forcing to minimum 1 second")
            seconds = 1
        
        self._timer_val_sec = hours * 60 * 60 + minutes * 60 + seconds
        self._timer_remaining_sec = self._timer_val_sec
        self._last_checked_time = time.time()
        logger.info("timer set to %d:%d:%d", hours, minutes, seconds)

    def reset(self):
        """reset the timer back to its original value.  If running, keeps running."""
        self._timer_remaining_sec = self._timer_val_sec
        self._last_checked_time = time.time()
        logger.info("%d:%d:%d timer reset", *self.calc_time(self._timer_val_sec))

    def start(self):
        """starts/restarts the timer, must call update() to update the timer and check for timer expired"""
        self.reset()
        self._running = True
        self._last_checked_time = time.time()
        # *(tuple) unpacks into positional args, and calc_time() returns a (h, m, s) tuple
        logger.info("%d:%d:%d timer started", *self.calc_time(self._timer_val_sec))
            
    def update(self) -> bool:
        """Call this as frequntly or as infrequently as you like.  Will return False if the timer expires"""
        if self._running:
            elapsed = time.time() - self._last_checked_time
            self._timer_remaining_sec = self._timer_remaining_sec - elapsed
            self._last_checked_time = time.time()
            if self._timer_remaining_sec <= 0:
                logger.info("%d:%d:%d timer elapsed", *self.calc_time(self._timer_val_sec))
                self._running = False
        if not self._running and self._timer_remaining_sec <= 0:
            return False
        else:
            return True
        
    def is_expired(self) -> bool:
        """If the timer has expired returns True.  Updates the timer when called"""
        if not self.update():
            return True
        else:
            return False
        
    def resume(self):
        self._running = True
        self._last_checked_time = time.time()
        logger.info("%d:%d:%d timer resumed with %d:%d:%d remaining", *self.calc_time(self._timer_val_sec), *self.calc_time(self._timer_remaining_sec))

    def stop(self):
        """stops / pauses the timer.  Start restarts it, resume continues where it left off"""
        self.update() # if update is not called before pause, make sure to capture whatever time was missed
        if self._running:
            self._running = False
        logger.info("%d:%d:%d timer stopped with %d:%d:%d remaining", *self.calc_time(self._timer_val_sec), *self.calc_time(self._timer_remaining_sec))

    def calc_time(self, seconds) -> tuple:
        """convert the raw seconds into (hours, minutes, seconds)"""
        # double // is integer division
        hours = seconds // (60 * 60)
        seconds = seconds - (hours * 60 * 60)
        minutes = seconds // 60
        # display nearest second, instead of floor() (which is the default for int display) to prevent confusion
        seconds = int(seconds - (minutes * 60))
        return (int(hours), int(minutes), int(seconds))

    def get_dict(self) -> dict:
        timer = {}
        timer["running"] = self._running
        timer["timer_val_sec"] = self._timer_val_sec
        timer["timer_remaining_sec"] = self._timer_remaining_sec
        timer["name"] = self._name
        timer["message"] = self._message
        timer["id"] = self._id
        timer["id_next"] = self._id_next
        timer["alarm_id"] = self._alarm_id
        return timer
    
    def _update_from_dict(self, timer: dict = None):
        """update the internals via a dict, used to restore a saved timer.  Performs validation on all fields.  Restoring a running timer is not supported and is always set to False"""
        logger.info("restoring timer from dict: %s", str(timer))
        
        if not isinstance(timer["running"], bool):
            logger.error("invalid timer running setting: %s", str(timer["running"]))
            raise ValueError

        if timer["running"] == True:
            logger.warn("attempted to restore a running saved timer, running set to False")
        self._running = False

        if not isinstance(timer["timer_val_sec"], int):
            logger.Error("invalid timer val type: %s",  type(timer["timer_val_sec"]))
            raise ValueError
        
        if timer["timer_val_sec"] < 0:
            logger.Error("invalid timer val second: %d",  timer["timer_val_sec"])
            raise ValueError
        
        self._timer_val_sec = timer["timer_val_sec"]

        if not isinstance(timer["timer_remaining_sec"], int) and not isinstance(timer["timer_remaining_sec"], float):
            logger.Error("invalid timer remaining val type: %s",  type(timer["timer_remaining_sec"]))
            raise ValueError
        
        if timer["timer_remaining_sec"] < 0:
            self._timer_remaining_sec = 0
        else:
            self._timer_remaining_sec = timer["timer_remaining_sec"]
        
        if not isinstance(timer["name"], str):
            self.error("Expecting name field to be string, got %s", type(timer["name"]))
            raise ValueError
        
        self._name = timer["name"]

        if not isinstance(timer["message"], str):
            self.error("Expecting message field to be string, got %s", type(timer["message"]))
            raise ValueError
        
        self._message = timer["message"]

        if timer["id"] != None:
            try:
                uuid_obj = uuid.UUID(timer["id"], version=4)
            except ValueError:
                logger.error("attempted to set invalid UUID %s as id", timer["id"])
                raise ValueError
            self._id = timer["id"]
        else:
            self._id = None

        # run these through the setter to confirm they are valid uuids
        self.id_next = timer["id_next"]
        self.alarm_id = timer["alarm_id"]
    
    # make the following properties read-only
    @property
    def running(self) -> bool:
        return self._running
    
    @property
    def timer_val_sec(self) -> int:
        return self._timer_val_sec
    
    @property
    def timer_remaining_sec(self) -> typing.Union[int, float]:
        return self._timer_remaining_sec

    @property
    def last_checked_time(self) -> float:
        return self._last_checked_time
    
    @property
    def id(self) -> str:
        return self._id
    
    # make the following properties read / write
    @property
    def name(self) -> str:
        return self._name
    
    @name.setter
    def name(self, value):
        if not isinstance(value, str):
            raise ValueError
        self._name = value
    
    @property
    def message(self) -> str:
        return self._message
    
    @message.setter
    def message(self, value):
        if not isinstance(value, str):
            raise ValueError
        self._message = value

    @property
    def alarm_id(self):
        return self._alarm_id
    
    @alarm_id.setter
    def alarm_id(self, value) -> typing.Union[str, None]:
        if value != None:
            try:
                uuid_obj = uuid.UUID(value, version=4)
            except ValueError:
                logger.error("attempted to set invalid UUID %s as id_next", value)
                raise ValueError
        self._alarm_id = value
    
    @property
    def id_next(self) -> typing.Union[str, None]:
        return self._id_next
    
    @id_next.setter
    def id_next(self, value: str):
        if value != None:
            try:
                uuid_obj = uuid.UUID(value, version=4)
            except ValueError:
                logger.error("attempted to set invalid UUID %s as id_next", value)
                raise ValueError
        self._id_next = value
    
    def __repr__(self) -> str:
        return self.get_dict().__repr__()

class Timer_Manager():
    def __init__(self):
        self._timer_list = []

    def add_timer(self, timer: Timer):
        """Add timer to the timer queue, and links previous timer to the new timer if one exists.  The new timer will be set to link to None.  Return True on success"""
        # if the number of timers in the list is greater than 1 and the previous timer is not linked, link this timer
        if len(self._timer_list) > 1:
            # check last timer in the list for link to next timer
            if self._timer_list[-1].id_next == None:
                # last timer not linked, create link
                self._timer_list[-1].id_next = timer.id
            else:
                logger.warn("Attempted to add timer %s when previous timer was already linked: %s", timer, self._timer_list[-1])
                return False
        self._timer_list.append(timer)
        return True

    def get_all_uuids(self):
        """return a list of all uuids in the Timer_Manager"""
        uuids = ()
        for timer in self._timer_list:
            uuids.append(timer.id)
        return uuids

if __name__ == "__main__":
    verbosity = 1
    sh = logging.StreamHandler()
    sh.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(name)s:%(levelname)s: %(message)s')
    sh.setFormatter(formatter)

    logger.addHandler(sh)
    logger.setLevel(verbosity*10)

    timer = Timer(0, 0, 3, "foo", "this is the message")
    timer.start()
    time.sleep(1)
    timer.stop()
    timer.reset()
    time.sleep(2)
    timer.resume()
    timer.set(0, 0, 4)
    while not timer.is_expired():
        print("timer is expired: {}".format(str(timer.is_expired())))
        time.sleep(1)
    print("timer is expired: {}".format(str(timer.is_expired())))
    print("Done! Timer dump")
    print(timer)

    timer.start()
    while timer.update():
        time.sleep(0.1)
    print("timer is expired: {}".format(str(timer.is_expired())))

    timer.start()
    time.sleep(1)
    timer.stop()
    timer.start()
    while timer.update():
        time.sleep(0.1)