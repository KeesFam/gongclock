import time
import zmq
import logging

logger = logging.getLogger(__name__)

context = zmq.Context()


def client():
    # socket to talk to server
    logger.info("connecting to hello world server ...")
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://localhost:5555")
    #socket.connect("ipc:///tmp/gong_timer_server_0")

    # do a couple of requests, waiting each time for a response
    for request in range(10):
        logger.info("sending request number %d", request)
        socket.send(b"Hello, ")

        # get the reply
        message = socket.recv()
        logger.info("received reply %d : %s", request, message)


if __name__ == "__main__":
    verbosity = 1
    sh = logging.StreamHandler()
    sh.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(name)s:%(levelname)s: %(message)s')
    sh.setFormatter(formatter)

    logger.addHandler(sh)
    logger.setLevel(verbosity*10)

    client()
