from setuptools import setup

setup(
    name = "gong_timer_server",
    version = "0.1.0",
    py_modules =
        [
            #'Timer'
            #'gong_timer_server',
            #'gong_timer_watcher',
            'gong_timer_cli'
        ],
    install_requires =
        [
            "click>=7.1.2",
            "pyzmq",
            "flask"
        ],
    entry_points =
      '''
          [console_scripts]
          gong_timer=gong_timer_cli:cli
      ''',
    )
