import logging
import click
import sys
import json
import zmq

logger = logging.getLogger(__name__)

@click.group()
@click.option('--verbosity', '-v', type=int, default= 3, help="specifies the verbosity level\n"
              "1 = debug\n2 = info\n3 = warning\n4 = error\n5 = critical")
@click.pass_context
def cli(ctx, verbosity):
    """Driver for activating gong events"""

    sh = logging.StreamHandler()
    sh.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(name)s:%(levelname)s: %(message)s')
    sh.setFormatter(formatter)

    logger.addHandler(sh)
    logger.setLevel(verbosity*10)

    if ctx.obj == None:
        ctx.obj = {}

    context = zmq.Context.instance()

    # socket to talk to server
    logger.info("connecting to gong_timer server ...")
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://localhost:5555")
    logger.info("connected")

    ctx.obj['sh'] = sh # stream handler for logging
    ctx.obj['verbosity'] = verbosity
    ctx.obj['zmq_ctx'] = context
    ctx.obj['zmq_req_rep_socket'] = socket

@click.command()
@click.option('--duration', '-d', type=int, default=100, show_default=True, help="Activate the PWM channel for this many milliseconds")
@click.option('--duty-cycle', '-p', type=int, default=100, show_default=True, help="Set the PWM duty cycle precentage [0-100]")
@click.option('--pwm-channel', '-c', type=int, default=0, show_default=True, help="Specify the PWM channel to use: 0=GPIO12, 1=GPIO13")
@click.option('--pwm-freq', '-f', type=int, default=1000, show_default=True, help="Specify the PWM channel frequency in Hz]")
@click.pass_context
def pulse(ctx, duration, duty_cycle, pwm_channel, pwm_freq):
    """activate the solenoid for the specified duration and duty cycle"""

    zmq_ctx = ctx.obj["zmq_ctx"]
    socket = ctx.obj["zmq_req_rep_socket"]

    try:
        logger.info("sending pulse request")
        args = {"duration_ms" : duration, "duty_cycle" : duty_cycle, "channel" : pwm_channel, "freq_hz" : pwm_freq}
        logger.debug("%s", str(args))
        socket.send_multipart(["pulse".encode("utf-8"), json.dumps(args).encode("utf-8")])

        # get the reply
        message = socket.recv()
        logger.info("received reply : %s", message)
    except Exception as e:
        if ctx.obj['verbosity'] > 2:
            click.echo("There was an error.  Re-run the command with the -v/--verbosity option set to 1 for more information:", err=True)
        click.echo(f"{e}", err=True)
    finally:
        socket.close()
        zmq_ctx.term()

@click.command()
@click.option('--pwm-channel', '-c', type=int, default=0, show_default=True, help="Specify the PWM channel to use: 0=GPIO12, 1=GPIO13")
@click.pass_context
def mute(ctx, pwm_channel):
    """activate the mute action on the specified channel"""

    zmq_ctx = ctx.obj["zmq_ctx"]
    socket = ctx.obj["zmq_req_rep_socket"]

    try:
        logger.info("sending mute request")
        args = {"channel" : pwm_channel}
        logger.debug("%s", str(args))
        socket.send_multipart(["mute".encode("utf-8"), json.dumps(args).encode("utf-8")])

        # get the reply
        message = socket.recv()
        logger.info("received reply : %s", message)
    except Exception as e:
        if ctx.obj['verbosity'] > 2:
            click.echo("There was an error.  Re-run the command with the -v/--verbosity option set to 1 for more information:", err=True)
        click.echo(f"{e}", err=True)
    finally:
        socket.close()
        zmq_ctx.term()

@click.command()
@click.pass_context
def flush(ctx):
    """flush all pending actions from the driver"""

    zmq_ctx = ctx.obj["zmq_ctx"]
    socket = ctx.obj["zmq_req_rep_socket"]

    try:
        logger.info("sending flush request")
        args = {}
        socket.send_multipart(["flush".encode("utf-8"), json.dumps(args).encode("utf-8")])

        # get the reply
        message = socket.recv()
        logger.info("received reply : %s", message)
    except Exception as e:
        if ctx.obj['verbosity'] > 2:
            click.echo("There was an error.  Re-run the command with the -v/--verbosity option set to 1 for more information:", err=True)
        click.echo(f"{e}", err=True)
    finally:
        socket.close()
        zmq_ctx.term()

@click.command()
@click.option('--volume', '-v', type=int, default=5, show_default=True, help="set to volume from 0 - 10")
@click.option('--pwm-channel', '-c', type=int, default=0, show_default=True, help="Specify the PWM channel to use: 0=GPIO12, 1=GPIO13")
@click.pass_context
def gong(ctx, volume, pwm_channel):
    """activate the preprogrammed gong action on the specified channel"""

    zmq_ctx = ctx.obj["zmq_ctx"]
    socket = ctx.obj["zmq_req_rep_socket"]

    try:
        logger.info("sending mute request")
        args = {"volume": volume, "channel" : pwm_channel}
        logger.debug("%s", str(args))
        socket.send_multipart(["gong".encode("utf-8"), json.dumps(args).encode("utf-8")])

        # get the reply
        message = socket.recv()
        logger.info("received reply : %s", message)
    except Exception as e:
        if ctx.obj['verbosity'] > 2:
            click.echo("There was an error.  Re-run the command with the -v/--verbosity option set to 1 for more information:", err=True)
        click.echo(f"{e}", err=True)
    finally:
        socket.close()
        zmq_ctx.term()

@click.command()
@click.pass_context
def start(ctx):
    """Start the timer"""

    zmq_ctx = ctx.obj["zmq_ctx"]
    socket = ctx.obj["zmq_req_rep_socket"]

    try:
        logger.info("sending start request")
        args = {}
        socket.send_multipart(["start".encode("utf-8"), json.dumps(args).encode("utf-8")])

        # get the reply
        message = socket.recv()
        logger.info("received reply : %s", message)
    except Exception as e:
        if ctx.obj['verbosity'] > 2:
            click.echo("There was an error.  Re-run the command with the -v/--verbosity option set to 1 for more information:", err=True)
        click.echo(f"{e}", err=True)
    finally:
        socket.close()
        zmq_ctx.term()

@click.command()
@click.pass_context
def stop(ctx):
    """Stop the timer"""

    zmq_ctx = ctx.obj["zmq_ctx"]
    socket = ctx.obj["zmq_req_rep_socket"]

    try:
        logger.info("sending stop request")
        args = {}
        socket.send_multipart(["stop".encode("utf-8"), json.dumps(args).encode("utf-8")])

        # get the reply
        message = socket.recv()
        logger.info("received reply : %s", message)
    except Exception as e:
        if ctx.obj['verbosity'] > 2:
            click.echo("There was an error.  Re-run the command with the -v/--verbosity option set to 1 for more information:", err=True)
        click.echo(f"{e}", err=True)
    finally:
        socket.close()
        zmq_ctx.term()

@click.command()
@click.pass_context
def resume(ctx):
    """Resume the timer"""

    zmq_ctx = ctx.obj["zmq_ctx"]
    socket = ctx.obj["zmq_req_rep_socket"]

    try:
        logger.info("sending resume request")
        args = {}
        socket.send_multipart(["resume".encode("utf-8"), json.dumps(args).encode("utf-8")])

        # get the reply
        message = socket.recv()
        logger.info("received reply : %s", message)
    except Exception as e:
        if ctx.obj['verbosity'] > 2:
            click.echo("There was an error.  Re-run the command with the -v/--verbosity option set to 1 for more information:", err=True)
        click.echo(f"{e}", err=True)
    finally:
        socket.close()
        zmq_ctx.term()

@click.command()
@click.pass_context
def reset(ctx):
    """Reset the timer"""

    zmq_ctx = ctx.obj["zmq_ctx"]
    socket = ctx.obj["zmq_req_rep_socket"]

    try:
        logger.info("sending reset request")
        args = {}
        socket.send_multipart(["reset".encode("utf-8"), json.dumps(args).encode("utf-8")])

        # get the reply
        message = socket.recv()
        logger.info("received reply : %s", message)
    except Exception as e:
        if ctx.obj['verbosity'] > 2:
            click.echo("There was an error.  Re-run the command with the -v/--verbosity option set to 1 for more information:", err=True)
        click.echo(f"{e}", err=True)
    finally:
        socket.close()
        zmq_ctx.term()

@click.command()
@click.option('--hours', '-h', type=int, default=0, show_default=True, help="Hours for timer")
@click.option('--minutes', '-m', type=int, default=0, show_default=True, help="Minutes for timer")
@click.option('--seconds', '-s', type=int, default=0, show_default=True, help="Seconds for timer")
@click.option('--name', '-n', type=str, default="", show_default=True, help="Name of the timer")
@click.option('--message', '-a', type=str, default="", show_default=True, help="Message displayed when completed")
@click.pass_context
def set_timer(ctx, hours, minutes, seconds, name, message):
    """set the timer value"""

    zmq_ctx = ctx.obj["zmq_ctx"]
    socket = ctx.obj["zmq_req_rep_socket"]

    try:
        if name != "":
            logger.info("sending set_name request")
            args = {"name" : name}
            logger.debug("%s", str(args))
            socket.send_multipart(["set_name".encode("utf-8"), json.dumps(args).encode("utf-8")])

            # get the reply
            reply = socket.recv()
            logger.info("received reply : %s", reply)
        
        if message != "":
            logger.info("sending set_message request")
            args = {"message" : message}
            logger.debug("%s", str(args))
            socket.send_multipart(["set_message".encode("utf-8"), json.dumps(args).encode("utf-8")])

            # get the reply
            reply = socket.recv()
            logger.info("received reply : %s", reply)

        if (hours + minutes + seconds) != 0:

            logger.info("sending set_timer request")
            args = {"hours" : hours, "minutes" : minutes, "seconds" : seconds}
            logger.debug("%s", str(args))
            socket.send_multipart(["set_timer".encode("utf-8"), json.dumps(args).encode("utf-8")])

            # get the reply
            reply = socket.recv()
            logger.info("received reply : %s", reply)
    except Exception as e:
        if ctx.obj['verbosity'] > 2:
            click.echo("There was an error.  Re-run the command with the -v/--verbosity option set to 1 for more information:", err=True)
        click.echo(f"{e}", err=True)
    finally:
        socket.close()
        zmq_ctx.term()

@click.command()
@click.pass_context
def get_timer(ctx):
    """Get the timer details"""

    zmq_ctx = ctx.obj["zmq_ctx"]
    socket = ctx.obj["zmq_req_rep_socket"]

    try:
        logger.info("sending get request")
        args = {}
        socket.send_multipart(["get".encode("utf-8"), json.dumps(args).encode("utf-8")])

        # get the reply
        message = socket.recv()
        timer_dict = json.loads(message.decode("utf-8"))
        logger.info("received reply : %s", str(timer_dict))
        click.echo(f"{timer_dict}")
    except Exception as e:
        if ctx.obj['verbosity'] > 2:
            click.echo("There was an error.  Re-run the command with the -v/--verbosity option set to 1 for more information:", err=True)
        click.echo(f"{e}", err=True)
    finally:
        socket.close()
        zmq_ctx.term()

@click.command()
@click.option('--volume', '-v', type=int, default=5, show_default=True, help="set alert (Timer elapsed) volume from 1 - 10")
@click.pass_context
def set_alert_volume(ctx, volume):
    """Set the timer alert volume setting"""

    zmq_ctx = ctx.obj["zmq_ctx"]
    socket = ctx.obj["zmq_req_rep_socket"]

    try:
        logger.info("sending set_alert_volume request")
        args = {"volume": volume}
        logger.debug("%s", str(args))
        socket.send_multipart(["set_alert_volume".encode("utf-8"), json.dumps(args).encode("utf-8")])

        # get the reply
        message = socket.recv()
        logger.info("received reply : %s", message)
    except Exception as e:
        if ctx.obj['verbosity'] > 2:
            click.echo("There was an error.  Re-run the command with the -v/--verbosity option set to 1 for more information:", err=True)
        click.echo(f"{e}", err=True)
    finally:
        socket.close()
        zmq_ctx.term()

@click.command()
@click.pass_context
def get_alert_volume(ctx):
    """Get the timer alert volume setting"""

    zmq_ctx = ctx.obj["zmq_ctx"]
    socket = ctx.obj["zmq_req_rep_socket"]

    try:
        logger.info("sending get request")
        args = {}
        socket.send_multipart(["get_alert_volume".encode("utf-8"), json.dumps(args).encode("utf-8")])

        # get the reply
        message = socket.recv()
        volume_dict = json.loads(message.decode("utf-8"))
        logger.info("received reply : %s", str(volume_dict))
        click.echo(f"{volume_dict['volume']}")
    except Exception as e:
        if ctx.obj['verbosity'] > 2:
            click.echo("There was an error.  Re-run the command with the -v/--verbosity option set to 1 for more information:", err=True)
        click.echo(f"{e}", err=True)
    finally:
        socket.close()
        zmq_ctx.term()

cli.add_command(pulse)
cli.add_command(mute)
cli.add_command(flush)
cli.add_command(gong)

cli.add_command(start)
cli.add_command(stop)
cli.add_command(resume)
cli.add_command(set_timer)
cli.add_command(reset)
cli.add_command(get_timer)
cli.add_command(set_alert_volume)
cli.add_command(get_alert_volume)

if __name__ == "__main__":
    cli()
