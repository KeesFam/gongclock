import pytest
from .. import Timer
import logging

from unittest.mock import patch
@pytest.fixture
def mock_time():
    with patch("time.time", return_value=0) as mock_time:
        yield mock_time

class TestTimer():
    def test_start(self, mock_time, caplog):
        # caplog.set_level(logging.DEBUG) # example of how to set the log capture to something higher than warn
        timer = Timer.Timer(0, 0, 1)
        timer.start()
        assert timer.running == True
        assert timer.last_checked_time == 0
        assert timer.is_expired() == False
    
    def test_update(self, mock_time):
        timer = Timer.Timer(0, 0, 1, "foo", "the message")
        timer.start()
        mock_time.return_value = 1
        assert timer.update() == False
        assert timer.running == False
        assert timer.timer_remaining_sec <= 0

    def test_is_expired(self, mock_time):
        timer = Timer.Timer(0, 0, 1, "foo", "the message")
        timer.start()
        mock_time.return_value = 1
        assert timer.is_expired() == True
        assert timer.running == False
        assert timer.timer_remaining_sec <= 0

    def test_stop(self, mock_time):
        timer = Timer.Timer(0, 0, 1, "foo", "the message")
        timer.start()
        mock_time.return_value = 0.5
        timer.stop()
        assert timer.is_expired() == False
        assert timer.running == False
        assert timer.timer_remaining_sec == 0.5

    def test_stop_resume(self, mock_time):
        timer = Timer.Timer(0, 0, 1, "foo", "the message")
        timer.start()
        mock_time.return_value = 0.5
        timer.stop()
        mock_time.return_value = 1
        assert timer.is_expired() == False
        assert timer.running == False
        assert timer.timer_remaining_sec == 0.5

        timer.resume()
        mock_time.return_value = 1.5
        assert timer.is_expired() == True
        assert timer.running == False
        assert timer.timer_remaining_sec == 0

    def test_reset_midway(self, mock_time):
        timer = Timer.Timer(0, 0, 1, "foo", "the message")
        timer.start()
        mock_time.return_value = 0.5
        timer.reset()
        assert timer.is_expired() == False
        assert timer.running == True
        assert timer.timer_remaining_sec == 1

        mock_time.return_value = 1.5
        assert timer.is_expired() == True
        assert timer.running == False
        assert timer.timer_remaining_sec <= 0

    def test_set_midway(self, mock_time):
        timer = Timer.Timer(0, 0, 1, "foo", "the message")
        timer.start()
        mock_time.return_value = 0.5
        timer.set(0, 0, 2)
        assert timer.is_expired() == False
        assert timer.running == True
        assert timer.timer_remaining_sec == 2

        mock_time.return_value = 2.5
        assert timer.is_expired() == True
        assert timer.running == False
        assert timer.timer_remaining_sec <= 0

    def test_stop_start(self, mock_time):
        timer = Timer.Timer(0, 0, 1, "foo", "the message")
        timer.start()
        mock_time.return_value = 0.5
        timer.stop()
        assert timer.is_expired() == False
        assert timer.running == False
        assert timer.timer_remaining_sec == 0.5

        mock_time.return_value = 1
        timer.start()
        assert timer.is_expired() == False
        assert timer.running == True
        assert timer.timer_remaining_sec == 1

        mock_time.return_value = 2
        assert timer.is_expired() == True
        assert timer.running == False
        assert timer.timer_remaining_sec == 0

    def test_1_minute(self, mock_time):
        timer = Timer.Timer(0, 1, 0, "foo", "the message")
        timer.start()
        assert timer.running == True
        assert timer.timer_remaining_sec == 60

        mock_time.return_value = 59
        assert timer.is_expired() == False
        assert timer.running == True
        assert timer.timer_remaining_sec == 1

        mock_time.return_value = 60
        assert timer.is_expired() == True
        assert timer.running == False
        assert timer.timer_remaining_sec == 0

    def test_1_hour(self, mock_time):
        timer = Timer.Timer(1, 0, 0, "foo", "the message")
        timer.start()
        assert timer.running == True
        assert timer.timer_remaining_sec == 60 * 60

        mock_time.return_value = 60 * 60 - 1
        assert timer.is_expired() == False
        assert timer.running == True
        assert timer.timer_remaining_sec == 1

        mock_time.return_value = 60 * 60
        assert timer.is_expired() == True
        assert timer.running == False
        assert timer.timer_remaining_sec == 0

    def test_5_hour_37_minute_16_second(self, mock_time):
        timer = Timer.Timer(5, 37, 16, "foo", "the message")
        timer.start()
        assert timer.running == True
        assert timer.timer_remaining_sec == 5 * 60 * 60 + 37 * 60 + 16

        mock_time.return_value = 5 * 60 * 60 + 37 * 60 + 16 - 1
        assert timer.is_expired() == False
        assert timer.running == True
        assert timer.timer_remaining_sec == 1

        mock_time.return_value = 5 * 60 * 60 + 37 * 60 + 16
        assert timer.is_expired() == True
        assert timer.running == False
        assert timer.timer_remaining_sec == 0

    def test_set_something_not_allowed(self):
        timer = Timer.Timer(5, 37, 16, "foo", "the message")
        with pytest.raises(AttributeError) as e_info:
            timer.id = "whoops"

    def test_set_something_allowed(self):
        timer = Timer.Timer(5, 37, 16, "foo", "the message")
        timer.name = "whoops"
        assert timer.name == "whoops"

    def test_set_id_next_invalid(self):
        timer = Timer.Timer()
        with pytest.raises(ValueError) as e_info:
            timer.id_next = "whoops"

    def test_set_id_next_to_none(self):
        timer = Timer.Timer()
        timer.id_next = None
        assert timer.id_next == None

    def test_set_id_next_to_valid_uuid(self):
        timer = Timer.Timer()
        test_uuid = '0894aa225d49490abd81cffc4547d6d1'
        timer.id_next = test_uuid
        assert timer.id_next == test_uuid
    
    def test_restore_from_dict_start(self, mock_time, caplog):
        caplog.set_level(logging.DEBUG) # example of how to set the log capture to something higher than warn

        saved_timer = {'running': False, 'timer_val_sec': 1, 'timer_remaining_sec': 1, 'name': 'foo', 'message': 'the message', 'id': 'd4c107cc2dd341cbbe7c93c22ac93a5e', 'id_next': None, 'alarm_id': None}
        timer = Timer.Timer()
        timer._update_from_dict(saved_timer)

        assert timer.running == False
        assert timer.timer_val_sec == 1
        assert timer.timer_remaining_sec == 1
        assert timer.name == "foo"
        assert timer.message == "the message"
        assert timer.id == 'd4c107cc2dd341cbbe7c93c22ac93a5e'
        assert timer.id_next == None
        assert timer.alarm_id == None

        # verify it runs correctly
        mock_time.return_value = 10

        timer.start()
        mock_time.return_value = 11
        assert timer.is_expired() == True
        assert timer.running == False
        assert timer.timer_remaining_sec == 0

    def test_restore_from_dict_resume(self, mock_time, caplog):
        caplog.set_level(logging.DEBUG) # example of how to set the log capture to something higher than warn

        saved_timer = {'running': False, 'timer_val_sec': 1, 'timer_remaining_sec': 0.5, 'name': 'foo', 'message': 'the message', 'id': 'd4c107cc2dd341cbbe7c93c22ac93a5e', 'id_next': None, 'alarm_id': None}
        timer = Timer.Timer()
        timer._update_from_dict(saved_timer)

        assert timer.running == False
        assert timer.timer_val_sec == 1
        assert timer.timer_remaining_sec == 0.5
        assert timer.name == "foo"
        assert timer.message == "the message"
        assert timer.id == 'd4c107cc2dd341cbbe7c93c22ac93a5e'
        assert timer.id_next == None
        assert timer.alarm_id == None

        # verify it runs correctly
        mock_time.return_value = 10

        timer.resume()
        mock_time.return_value = 11
        assert timer.is_expired() == True
        assert timer.running == False
        assert timer.timer_remaining_sec == -0.5