#!/bin/bash

set -e # exit on any nonzero return value
set -x # echo each command as its being run

pushd /home/gong/gongclock/timer_server/
sudo python3 -m venv venv
sudo chown -R $USER:$USER venv
. venv/bin/activate
python3 -m pip install pyzmq
python3 -m pip install --upgrade luma.oled
python3 -m pip install pytest
python3 -m pip install -e ../py_gong_driver/.
python3 -m pip install -e .
python3 -m pip install flask
deactivate

sudo cp gong_timer_server.service /usr/lib/systemd/system/.
sudo cp gong_timer_watcher.service /usr/lib/systemd/system/.
sudo systemctl daemon-reload
sudo systemctl enable --now gong_timer_server.service
sudo systemctl enable --now gong_timer_watcher.service

popd