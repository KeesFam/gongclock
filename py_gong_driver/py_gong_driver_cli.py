import logging
import click
import sys
import py_gong_driver
import time

logger = logging.getLogger(__name__)

@click.group()
@click.option('--verbosity', '-v', type=int, default= 3, help="specifies the verbosity level\n"
              "1 = debug\n2 = info\n3 = warning\n4 = error\n5 = critical")
@click.pass_context
def cli(ctx, verbosity):
    """Driver for activating gong events"""

    sh = logging.StreamHandler()
    sh.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(name)s:%(levelname)s: %(message)s')
    sh.setFormatter(formatter)

    logger.addHandler(sh)
    logger.setLevel(verbosity*10)

    driver_logger = logging.getLogger('py_gong_driver')
    driver_logger.addHandler(sh)
    driver_logger.setLevel(verbosity*10)

    if ctx.obj == None:
        ctx.obj = {}

    ctx.obj['sh'] = sh # stream handler for logging
    ctx.obj['verbosity'] = verbosity

@click.command()
@click.option('--duration', '-d', type=int, default=100, show_default=True, help="Activate the PWM channel for this many milliseconds")
@click.option('--duty-cycle', '-p', type=int, default=100, show_default=True, help="Set the PWM duty cycle precentage [0-100]")
@click.option('--pwm-channel', '-c', type=int, default=0, show_default=True, help="Specify the PWM channel to use: 0=GPIO12, 1=GPIO13")
@click.option('--pwm-freq', '-f', type=int, default=1000, show_default=True, help="Specify the PWM channel frequency in Hz]")
@click.pass_context
def pulse(ctx, duration, duty_cycle, pwm_channel, pwm_freq):
    """activate the solenoid for the specified duration and duty cycle"""

    try:
        driver = py_gong_driver.PulseDriver(pwm_freq)
        driver.pulse(duration, duty_cycle, pwm_channel)
        driver.shutdown()
        #py_gong_driver.pulse(duration, duty_cycle)
    except Exception as e:
        if ctx.obj['verbosity'] > 2:
            click.echo("There was an error.  Re-run the command with the -v/--verbosity option set to 1 for more information:", err=True)
        click.echo(f"{e}", err=True)
        sys.exit(1)

@click.command()
@click.option('--d-0', type=int, default=100, show_default=True, help="Drive PWM 0 for this many milliseconds")
@click.option('--d-1', type=int, default=100, show_default=True, help="Drive PWM 1 for this many milliseconds")
@click.option('--dc-0', type=int, default=100, show_default=True, help="Set the PWM 0 duty cycle precentage [0-100]")
@click.option('--dc-1', type=int, default=100, show_default=True, help="Set the PWM 1 duty cycle precentage [0-100]")
@click.option('--pwm-freq', '-f', type=int, default=1000, show_default=True, help="Specify the PWM channel frequency in Hz]")
@click.pass_context
def dual_pulse(ctx, d_0, d_1, dc_0, dc_1, pwm_freq):
    """activate both PWM channels for the specified duration and duty cycle simultaneously."""

    try:
        driver = py_gong_driver.PulseDriver(pwm_freq)
        #driver.pulse(d_0, dc_0, 0)
        #driver.pulse(d_1, dc_1, 1)
        driver.dual_pulse(d_0, dc_0, d_1, dc_1)
        driver.shutdown()
    except Exception as e:
        if ctx.obj['verbosity'] > 2:
            click.echo("There was an error.  Re-run the command with the -v/--verbosity option set to 1 for more information:", err=True)
        click.echo(f"{e}", err=True)
        sys.exit(1)

@click.command()
@click.pass_context
def flush_test(ctx):
    """attempt a long pulse, then flush the queue and try a short pulse"""

    try:
        driver = py_gong_driver.PulseDriver()
        driver.pulse(10_000, 50, 0)
        driver.pulse(10_000, 50, 1)
        driver.dual_pulse(5_000, 30, 2_000, 75)
        time.sleep(1)
        driver.flush()
        driver.pulse(500, 100, 0)
        driver.shutdown()
    except Exception as e:
        if ctx.obj['verbosity'] > 2:
            click.echo("There was an error.  Re-run the command with the -v/--verbosity option set to 1 for more information:", err=True)
        click.echo(f"{e}", err=True)
        sys.exit(1)

cli.add_command(pulse)
cli.add_command(dual_pulse)
cli.add_command(flush_test)

if __name__ == "__main__":
    cli()
