# Main CLI app and API for driving a gong connected to the raspberry pi

# Commands

# Installation

# edit `/boot/config.txt` and add
# `dtoverlay=pwm-2chan`
# => (PWM channel 0 = GPIO18, PWM channel 1 = GPIO19)
# or
# `dtoverlay=pwm-2chan,pin=12,func=4,pin2=13,func2=4`
# => (PWM channel 0 = GPIO12, PWM channel 1 = GPIO13)

`pip install -e .`

# Uninstallation

`pip uninstall py_gong_driver`
