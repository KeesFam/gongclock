from setuptools import setup
# for information on the "dll" (pyd file) install
# refer to https://docs.python.org/2/distutils/setupscript.html#installing-additional-files
setup(
    name = "pyGongDriver",
    version = "0.1.0",
    py_modules =
        [
            'py_gong_driver_cli'
            'py_gong_driver'
        ],
    install_requires =
        [
            "click>=7.1.2",
            "rpi_hardware_pwm",
        ],
    entry_points =
      '''
          [console_scripts]
          gong_driver=py_gong_driver_cli:cli
      ''',
    )
