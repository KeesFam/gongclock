import logging
import time
from rpi_hardware_pwm import HardwarePWM
import threading
import queue
import collections

logger = logging.getLogger(__name__)

_MIN_DUTY_CYCLE = 0
_MAX_DUTY_CYCLE = 100
_MIN_DURATION = 0
_MAX_DURATION = 10_000
_MIN_FREQ_HZ = 100
_MAX_FREQ_HZ = 10_000

_SYSTEM_VOLTAGE = 20

class PWM_Action(dict):
    def __init__(self, channel = 0, duty_cycle = 100, duration_ms = 100, freq_hz = 1000):

        if channel not in [0, 1]:
            logger.error("PWM channel not valid: %d", channel)
            raise ValueError
        else:
            self.channel = channel

        super().__setitem__("channel", self.channel)

        if duty_cycle < _MIN_DUTY_CYCLE:
            logger.warning("Duty cycle was lower than %d%%, setting to %d%%", _MIN_DUTY_CYCLE, _MIN_DUTY_CYCLE)
            self.duty_cycle = _MIN_DUTY_CYCLE
        elif duty_cycle > _MAX_DUTY_CYCLE:
            logger.warning("Duty cycle was higer than %d%%, setting to %d%%", _MAX_DUTY_CYCLE, _MAX_DUTY_CYCLE)
            self.duty_cycle = _MAX_DUTY_CYCLE
        else:
            self.duty_cycle = duty_cycle

        super().__setitem__("duty_cycle", self.duty_cycle)

        if duration_ms < _MIN_DURATION:
            logger.warning("Duration was less than %d ms, setting to %d ms", _MIN_DURATION, _MIN_DURATION)
            self.duration_ms = _MIN_DURATION
        #elif duration_ms > _MAX_DURATION:
        #    logger.warning("Duration was more than %d ms, setting to %d ms", _MAX_DURATION, _MAX_DURATION)
        #    self.duration_ms = _MAX_DURATION
        else:
            self.duration_ms = duration_ms

        super().__setitem__("duration_ms", self.duration_ms)

        if freq_hz < _MIN_FREQ_HZ:
            logger.warning("PWM frequency was lower than %d% Hz, setting to %d Hz", _MIN_FREQ_HZ, _MIN_FREQ_HZ)
            self.freq_hz = _MIN_FREQ_HZ
        elif freq_hz > _MAX_FREQ_HZ:
            logger.warning("PWM Frequency was higer than %d Hz, setting to %d Hz", _MAX_FREQ_HZ, _MAX_FREQ_HZ)
            self.freq_hz = _MAX_FREQ_HZ
        else:
            self.freq_hz = freq_hz

        super().__setitem__("freq_hz", self.freq_hz)


# PWM channel 0 worker thread
class PWM_Channel_Thread(threading.Thread):
    def __init__(self, workQueue, queueLock, freq_hz = 1000):
        logger.debug("Initializing thread")
        super(PWM_Channel_Thread, self).__init__()
        self.workQueue = workQueue
        self.queueLock = queueLock
        self.freq_hz = freq_hz
        self.exit_requested = False

        logger.info("Allocating and configuring PWM channel %d and frequency %d hz", 0, self.freq_hz)
        self.pwm_0 = HardwarePWM(pwm_channel = 0, hz=self.freq_hz)
        self.pwm_0.start(0)
        self.pwm_0_queue = queue.Queue()
        self.pwm_0_busy = False
        self.pwm_0_change_time = 0

        logger.info("Allocating and configuring PWM channel %d and frequency %d hz", 1, self.freq_hz)
        self.pwm_1 = HardwarePWM(pwm_channel = 1, hz=self.freq_hz)
        self.pwm_1.start(0)
        self.pwm_1_queue = queue.Queue()
        self.pwm_1_busy = False
        self.pwm_1_change_time = 0

        logger.info("PWM channels started")

    def run(self):
        work_obj = []
        logger.info("Starting PWM_Channel_Thread")
        while True:
            with self.queueLock:
                if not self.workQueue.empty():
                    work_obj = self.workQueue.get()
                    logger.debug(f"Thread got something: {work_obj}")
                    if isinstance(work_obj, list):
                        logger.debug("Thread got list of work")
                        for obj in work_obj:
                            if isinstance(obj, PWM_Action):
                                self.add_work_to_pwm_queues(obj)
                            else:
                                logger.error("Thread Encountered unexpected work object, exiting")
                                break

                    elif isinstance(work_obj, PWM_Action):
                        logger.debug("Thread got single work")
                        self.add_work_to_pwm_queues(work_obj)

                    elif isinstance(work_obj, str):
                        if work_obj == 'kill':
                            logger.warn("Got Kill! Exiting PWM_Channel_Thread PWM Channel NOW!")
                            self.pwm_0.stop()
                            self.pwm_1.stop()
                            break
                        elif work_obj == 'quit':
                            logger.info("Requesting thread exit when all work complete")
                            self.exit_requested = True
                        elif work_obj == 'flush':
                            logger.info("Thread flush requested, discarding all pending actions and cancelling all current actions")
                            while not self.pwm_0_queue.empty():
                                toss = self.pwm_0_queue.get()
                                logger.debug("Tossing action: %s", toss)
                            self.pwm_0_change_time = time.time() # force any current action to be done now
                            # put zero duty cycle action in now for immediate action
                            self.pwm_0_queue.put(PWM_Action(duty_cycle = 0, duration_ms = 0, channel = 0))

                            while not self.pwm_1_queue.empty():
                                toss = self.pwm_1_queue.get()
                                logger.debug("Tossing action: %s", toss)
                            self.pwm_1_change_time = time.time() # force any current action to be done now
                            # put zero duty cycle action in now for immediate action
                            self.pwm_1_queue.put(PWM_Action(duty_cycle = 0, duration_ms = 0, channel = 0))
                        else:
                            logger.error("Thread Encountered unexpected work object string in PWM_Channel_Thread : %s", work_obj)
                    else:
                        logger.error("Thread Encountered unexpected work object in PWM_Channel_Thread : %s", work_obj)
                        break
            # process the next timestep of PWM actions
            self.execute_pwm_actions()
            if self.exit_requested == True:
                if self.pwm_0_busy == False and self.pwm_1_busy == False:
                    logger.info("All work done, exiting thread.")
                    self.pwm_0.stop()
                    self.pwm_1.stop()
                    break
            time.sleep(0.001)

    def add_work_to_pwm_queues(self, work_obj):
        if work_obj["channel"] == 0:
            logger.debug("Put pwm_action: channel %d, duty %d%%, duration %d ms into pwm 0 queue", work_obj["channel"], work_obj["duty_cycle"], work_obj["duration_ms"])
            self.pwm_0_queue.put(work_obj)
        elif work_obj["channel"] == 1:
            logger.debug("Put pwm_action: channel %d, duty %d%%, duration %d ms into pwm 1 queue", work_obj["channel"], work_obj["duty_cycle"], work_obj["duration_ms"])
            self.pwm_1_queue.put(work_obj)
        else:
            logger.error("Thread channel and work object channel mismatch! Ignoring, but you probably dont want this.")

    def execute_pwm_actions(self):

        if self.pwm_0_busy == False:
            if not self.pwm_0_queue.empty():
                pwm_action = self.pwm_0_queue.get()
                logger.debug("Thread starting pwm 0 %d ms pulse at %d %% duty cycle", pwm_action["duration_ms"], pwm_action["duty_cycle"])
                self.pwm_0_change_time = time.time() + pwm_action["duration_ms"]/1000.0 # convert ms to seconds
                self.pwm_0.change_duty_cycle(pwm_action["duty_cycle"])
                self.pwm_0_busy = True
        else:
            if time.time() >= self.pwm_0_change_time:
                logger.debug("Thread pulse complete pwm 0")
                if not self.pwm_0_queue.empty():
                    pwm_action = self.pwm_0_queue.get()
                    logger.debug("Thread starting pwm 0 %d ms pulse at %d %% duty cycle", pwm_action["duration_ms"], pwm_action["duty_cycle"])
                    self.pwm_0_change_time = time.time() + pwm_action["duration_ms"]/1000.0 # convert ms to seconds
                    self.pwm_0.change_duty_cycle(pwm_action["duty_cycle"])
                    self.pwm_0_busy = True
                else:
                    self.pwm_0_busy = False


        if self.pwm_1_busy == False:
            if not self.pwm_1_queue.empty():
                pwm_action = self.pwm_1_queue.get()
                logger.debug("Thread starting pwm 1 %d ms pulse at %d %% duty cycle", pwm_action["duration_ms"], pwm_action["duty_cycle"])
                self.pwm_1_change_time = time.time() + pwm_action["duration_ms"]/1000.0 # convert ms to seconds
                self.pwm_1.change_duty_cycle(pwm_action["duty_cycle"])
                self.pwm_1_busy = True
        else:
            if time.time() >= self.pwm_1_change_time:
                logger.debug("Thread pulse complete pwm 1")
                if not self.pwm_1_queue.empty():
                    pwm_action = self.pwm_1_queue.get()
                    logger.debug("Thread starting pwm 1 %d ms pulse at %d %% duty cycle", pwm_action["duration_ms"], pwm_action["duty_cycle"])
                    self.pwm_1_change_time = time.time() + pwm_action["duration_ms"]/1000.0 # convert ms to seconds
                    self.pwm_1.change_duty_cycle(pwm_action["duty_cycle"])
                    self.pwm_1_busy = True
                else:
                    self.pwm_1_busy = False



# edit `/boot/config.txt` and add
# `dtoverlay=pwm-2chan`
# => (PWM channel 0 = GPIO18, PWM channel 1 = GPIO19)
# or
# `dtoverlay=pwm-2chan,pin=12,func=4,pin2=13,func2=4`
# => (PWM channel 0 = GPIO12, PWM channel 1 = GPIO13)
_PWM_CHANNEL = 0
_PWM_FREQ_HZ = 1000

class PulseDriver():

    def __init__(self, freq_hz = 1000):
        self.work_queue = queue.Queue()
        self.queue_lock = threading.Lock()
        self.freq_hz = freq_hz
        self.worker = PWM_Channel_Thread(self.work_queue, self.queue_lock, self.freq_hz)
        self.worker.setDaemon(True)
        self.worker.start()

    def pulse(self, duration = 100, duty_cycle = 100, channel = 0):

        logger.debug("pulsing for %d ms at %d%% duty cycle", duration, duty_cycle)

        action = PWM_Action(duty_cycle = duty_cycle, duration_ms = duration, channel = channel)
        self.add_pulse_to_queue(action)

        action = PWM_Action(duty_cycle = 0, duration_ms = 0, channel = channel)
        self.add_pulse_to_queue(action)

    def gong(self, volume = 5, channel = 0):
        """pre-baked gong based on volume and channel, volume is between 1 (softest) and 10 (loudest)"""

        if _SYSTEM_VOLTAGE == 20:
            # 20 volt gong range
            # gong range is 17 (very soft) to 30 (loud) with 100% duty cycle and 100 Hz freq
            volume_map = {
                # volume : (duration, duty cycle)
                1 : (17, 100),
                2 : (18,100),
                3 : (19,100),
                4 : (20,100),
                5 : (21,100),
                6 : (22,100),
                7 : (23,100),
                8 : (25,100),
                9 : (27,100),
                10 : (30,100),
            }

        else:
            # 12 volt gong volume range
            # gong range is 65 (very soft) to 95 (loud) with 100% duty cycle and 297 Hz freq
            # 64 is even softer, but doesn't always strike the gong
            volume_map = {
                # volume : (duration, duty cycle)
                1 : (71, 100),
                2 : (73,100),
                3 : (76,100),
                4 : (78,100),
                5 : (80,100),
                6 : (82,100),
                7 : (84,100),
                8 : (86,100),
                9 : (89,100),
                10 : (95,100),
            }

        if volume < 1:
            logger.warn("volume is set to less than 1, forcing to 1")
            volume = 1

        if volume > 10:
            logger.warn("volume is set to greater than 10, forcing to 10")
            volume = 10

        duration, duty_cycle = volume_map[volume]

        logger.debug("pulsing for %d ms at %d%% duty cycle", duration, duty_cycle)

        action = PWM_Action(duty_cycle = duty_cycle, duration_ms = duration, channel = channel)
        self.add_pulse_to_queue(action)

        action = PWM_Action(duty_cycle = 0, duration_ms = 0, channel = channel)
        self.add_pulse_to_queue(action)

    def mute(self, channel = 0):
        """pre-baked mute, flushes all pending pulses and mutes now"""

        self.flush()

        # 20 volt
        if _SYSTEM_VOLTAGE == 20:
            duration, duty_cycle = (5000, 50)

        else:
            # 12 volt
            duration, duty_cycle = (5000, 70)

        logger.debug("pulsing for %d ms at %d%% duty cycle", duration, duty_cycle)

        action = PWM_Action(duty_cycle = duty_cycle, duration_ms = duration, channel = channel)
        self.add_pulse_to_queue(action)

        action = PWM_Action(duty_cycle = 0, duration_ms = 0, channel = channel)
        self.add_pulse_to_queue(action)

    def dual_pulse(self, duration_0 = 100, duty_cycle_0 = 100, duration_1 = 100, duty_cycle_1 = 100):

        logger.debug("dual pulsing for %d / %d ms at %d / %d %% duty cycle", duration_0, duration_1, duty_cycle_0, duty_cycle_1)
        actions = []

        action = PWM_Action(duty_cycle = duty_cycle_0, duration_ms = duration_0, channel = 0)
        actions.append(action)
        action = PWM_Action(duty_cycle = duty_cycle_1, duration_ms = duration_1, channel = 1)
        actions.append(action)


        action = PWM_Action(duty_cycle = 0, duration_ms = 0, channel = 0)
        actions.append(action)
        action = PWM_Action(duty_cycle = 0, duration_ms = 0, channel = 1)
        actions.append(action)

        self.add_pulse_to_queue(actions)

    def add_pulse_to_queue(self, pwm_action):
        with self.queue_lock:
            logger.debug("putting pwm action into work queue")
            self.work_queue.put(pwm_action)

    def flush(self):
        logger.debug("flushing thread")
        with self.queue_lock:
            self.work_queue.put('flush')

    def shutdown(self):
        logger.debug("Gracefully quitting thread")
        with self.queue_lock:
            self.work_queue.put('quit')
        self.worker.join()
        logger.debug("Thread joined successfully")

    def kill(self):
        logger.debug("killing thread")
        with self.queue_lock:
            self.work_queue.put('kill')
        self.worker.join()
        logger.debug("Thread joined successfully")
