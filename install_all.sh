#!/bin/bash
set -e # exit on any nonzero return value
set -x # echo each command as its being run

pushd website
./install.sh
popd

pushd timer_server
./install.sh
popd