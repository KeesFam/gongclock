$fn=100;

gong_holder();

module gong_holder() {
color("khaki", 1.0) union(){
//vertical strut 5/8" in diameter, 13.25" from face of base to top
translate([36.5125, 19.05, 0]) cylinder(h = 336.55, d = 15.875);
translate([36.5125+28.575, 19.05, 0]) cylinder(h = 336.55, d = 15.875);
// side brace 3/4" diameter, 1/2" between bars + penetration
translate([36.5125, 19.05, 63.5]) rotate([0, 90, 0]) cylinder(h = 28.6512, d = 12.7);
// base 13 11/16" wide, 4" deep, 3/4" tall
translate([0, 0, -19.5]) cube([101.6, 347.6625, 19.5]);

//vertical strut 5/8" in diameter, 13.25" from face of base to top
translate([36.5125, 328.6125, 0]) cylinder(h = 336.55, d = 15.875);
translate([36.5125+28.575, 328.6125, 0]) cylinder(h = 336.55, d = 15.875);
// side brace 3/4" diameter, 1/2" between bars + penetration
translate([36.5125, 328.6125, 63.5]) rotate([0, 90, 0]) cylinder(h = 28.6512, d = 12.7);

// top bar 3/4" diameter 13 11/16" long
translate([50, 0, 307.975]) rotate([-90, 0,0]) cylinder(h = 347.662, d = 19.05);
};
};