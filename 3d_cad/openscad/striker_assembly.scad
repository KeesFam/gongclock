$fn = 100;

striker_assembly();

// top at 33.6 mm
module striker_assembly() {
translate([16.8, 0, 0 ]) rotate([0, 0, -90]) color("green") import("mallet-noid-craddle.stl");

translate([33.6, 0, 32.75]) rotate([-90, 0, -90]) union() {
color("red") import("mallet-noid-plate.stl");
translate([1.5, -37, -4.1]) rotate([0, -90, 0]) color("blue") import("mallet-noid-linkage.stl");
}
}
// probe
// translate([33.6, 0, 10]) rotate([0, 90, 0]) cylinder(h=5, d=10);