$fn = 100;

mallet();

module mallet() {
union(){
    translate([0, 0, 215.9]) rotate([180, 0, 0]) union(){
        // mallet shaft, 0.5" diameter, 8.5" tall
        color("PaleGoldenrod", 1.0) cylinder(h = 215.9, d = 12.7);
        // mallet head, 1" tall, 1.5" diameter
        translate([0, 0, -25.4]) color("white", 1.0) cylinder(h = 25.4, d = 38.1);
    };
};
};