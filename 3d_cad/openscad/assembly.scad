$fn = 100;

use <mallet.scad>
use <gong.scad>
use <gong_holder.scad>


// move the gong 2" forward, 13 11/16" /2 to the right, and 7 1.8" up
translate([50.8, 173.83125,  180.975]) gong();
translate([100, 0, 0]) rotate([-45, 0, 0]) mallet();
gong_holder();
