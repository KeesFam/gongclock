$fn = 100;

use <mallet.scad>
use <gong.scad>
use <gong_holder.scad>
use <striker_assembly.scad>

// move the gong 2" forward, 13 11/16" /2 to the right, and 7 1.8" up
//translate([50.8, 173.83125,  180.975]) gong();
//translate([130, 27, 0]) rotate([-45, 0, 0]) mallet();
//gong_holder();
// original box
//translate([0, 26.9875, 0]) magic_base();
//translate([0, 26.9875, 0]) striker_holder();
//translate([75, 45, 18]) rotate([-45, 0, 0]) striker_assembly();
//translate([75, 300, 18]) rotate([45, 0, 0]) striker_assembly();

module magic_base() {
color("purple") union() {
// 4" deep, 11.5625 " wide, and 2.5 " tall
// original box
//cube([101.6, 293.6875, 63.5]);
cube([75, 293.6875, 63.5]);

}
}

striker_holder();

module striker_holder() {
    
    cube([75, 75, 75]);
    //vertical strut 5/8" in diameter, 13.25" from face of base to top
    //translate([0,-26.9875,0]) difference(){
    cube([75, 40, 50]);
translate([36.5125, 19.05, 0]) cylinder(h = 100, d = 15.875);
translate([36.5125+28.575, 19.05, 0]) cylinder(h = 100, d = 15.875);

    
}